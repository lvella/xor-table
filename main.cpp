#include <iostream>
#include "XORTable.h"

#define BITS 5

using namespace std;

struct NodeRoutingInfo
{
    int delay;
};

typedef xortable::XORTable<BITS,NodeRoutingInfo> MyTable;

int main()
{
    //XOR Table for node with id 2.
    MyTable xortable(2);

    NodeRoutingInfo ri;
    ri.delay = 10;

    //Adding entries for does with IDs 4,5, and 6
    cout << xortable.add(4, ri) << endl;
    cout << xortable.add(4, ri) << endl;
    cout << xortable.add(5, ri) << endl;
    cout << xortable.add(6, ri) << endl;
    cout << xortable.add(5, ri) << endl;
    cout << xortable.add(6, ri) << endl;

    //Reference get:
    NodeRoutingInfo &ro = xortable.get(6);
    cout << ro.delay << endl;

    ro.delay = 20;

    //Copy made (from the updated version).
    NodeRoutingInfo ro2 = xortable.get(6);
    cout << ro2.delay << endl;

    //Another (unupdated) reference get:
    NodeRoutingInfo &ro3 = xortable.get(4);
    cout << ro3.delay << endl;

    return 0;
}
