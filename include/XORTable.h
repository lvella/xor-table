#ifndef XORTABLE_H
#define XORTABLE_H

#define DEBUG

#include <vector>
#include <map>
#include <bitset>
#include <cmath>
#include <stdexcept>
#include <iostream>

namespace xortable {

    //Leading 0 bits
    #define L0B(X,Y)   ((X==0)? Y: (Y - ((int)(log2(X))+1)))
    #define L0B1(X,Y)              (Y - ceil(log2(X+1)))



    template<int CodeBits,class T>
    class XORTable
    {
        public:
	    typedef typename std::bitset<CodeBits> NODE_ID;

            XORTable(NODE_ID id):
            myId(id)
            {
            }

            virtual ~XORTable(){};

            bool add(NODE_ID id, const T& routeInfo)
            {
                if(myId == id)
                    return false;

                unsigned long int no0bits = L0B((myId ^ id).to_ulong(), CodeBits);

		// A copy of the object will be made:
                bool result = headers[no0bits].insert(make_pair(id, routeInfo)).second;

#ifdef DEBUG
                if(result)
                    std::cout << "not found. adding" << std::endl;
                else
                    std::cout << "found. not adding" << std::endl;
#endif

                return result;
            }


            T& get(NODE_ID id)
            {
                unsigned long int no0bits = L0B((myId ^ id).to_ulong(), CodeBits);
                return headers[no0bits][id];
            }


        protected:
        private:
            struct NodeIdComparator
       	    { // simple comparison
                bool operator() (NODE_ID valueA,const NODE_ID valueB)
                {
                    return valueA.to_ulong() < valueB.to_ulong();
                }
            };

            NODE_ID myId;
            typedef typename std::map<NODE_ID, T, NodeIdComparator> HeadersMap;
            HeadersMap headers[CodeBits];
    };
}
#endif // XORTABLE_H
